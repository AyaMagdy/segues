//
//  ViewController.swift
//  Segues
//
//  Created by aya magdy on 2/26/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textFiield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func buttonPressed(_ sender: Any) {
        performSegue(withIdentifier: "goToSecondScreen", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToSecondScreen"{
            let destinationVC = segue.destination as! SecondViewController
            destinationVC.textPassedOver = textFiield.text
        }
    }
    
}

